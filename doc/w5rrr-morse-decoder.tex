\documentclass{amsart}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{float}
\usepackage{fullpage}
\graphicspath{ {imgs/} }
\begin{document}
\noindent

\title[Assembly Instructions]{W5RRR Morse Decoder Assembly Instructions}
\maketitle

\section{Introduction}

These instructions are for assembling the v1.0 W5RRR Morse Decoder board.
It is a through hole soldering project that, when properly assembled, provides morse decoding and keyer functionality.
It can take line level or microphone input of audio CW signals and convert them to text on the display.

\subsection{Terminology}
For the purpose of these instructions the \textit{front} of the board is the side with the large text "W5RRR Morse Decoder".
This is the side the display will mount on.

\subsection{Parts and Tools} The following will be needed for this project

\begin{itemize}
\item W5RRR CW Decoder Kit
\item Short lengths (~5-10cm) insulated wire
\item Soldering iron
\item Solder
\item Flush cutters
\end{itemize}

Recommended but not required:
\begin{itemize}
\item Tape
\item Voltmeter
\item Tweezers
\item Board holder or vice
\item Desoldering wick
\end{itemize}

\section{Power Supply}
\subsection{Install Power Supply Components}

Collect the following power supply components and install them on the back of the board.

You should install SW1 (the power switch) on the front of the board (instead of the back where the silkscreen is) or remote mount it if using an enclosure.

\begin{figure}[h!]
\includegraphics[width=0.7\textwidth]{ps-comps.jpg}
\centering
\end{figure}

\vspace{1em}
\begin{tabular}{ p{5cm}p{5.5cm}}
    \textbf{10$\mu$F Cap:} $\square$ C2, $\square$ C4&\textbf{0.1$\mu$F Cap:} $\square$ C5\\
    \textbf{Power Jack:} $\square$ J1&\textbf{7805 Volt. Regulator:} $\square$ IC1\\
    \textbf{Fuse:} $\square$ F1&\textbf{Power Switch:} $\square$ SW1\\
    \textbf{Protection Diode:} $\square$ D1\\
\end{tabular}
\vspace{1em}

Note that C2, C4, and D1 are polarized.
Silkscreen in marked accordingly.
Note the stripe with the minus signs on the side of the capacitors, and the ring on the diode marking the cathode.
D1 is a tight fit and may be installed vertically.

After soldering the components in place you may trim the leads close to the surface of the board with the flush cutters.
Trim as needed, usually after each step of this guide.

\begin{figure}[h!]
\includegraphics[width=0.7\textwidth]{ps-install.jpg}
\centering
\end{figure}

\subsection{Test Power Supply}

Test the power supply by applying 7-15V on J1 or the JP1 pads.
Ensure SW1 is in the on position (towards the center of the board).
You should measure about 5 Volts between IC1's tab and pin 3 as shown in the photo.
Be careful to avoid touching the probe to multiple pins on IC1 as it will cause a short circuit!

\begin{figure}[h!]
\includegraphics[width=0.7\textwidth]{ps-meas.jpg}
\centering
\end{figure}

\vspace{1em}
\begin{tabular}{ p{5cm}p{5.5cm}}
    $\square$ 5V measured at IC1 pin 3
\end{tabular}
\vspace{1em}

\newpage

\section{Main Assembly}
\subsection{Install Resistors}

Collect and install the following resistors. Values are also marked on silkscreen.

\begin{figure}[h!]
\includegraphics[width=0.6\textwidth]{resistors.jpg}
\centering
\end{figure}

\vspace{1em}
\begin{tabular}{ p{6.5cm}p{5.5cm}}
    \textbf{100k$\Omega$:} $\square$ R17, $\square$ R18, $\square$ R19&\textbf{27$\Omega$:} $\square$ R3\\
    \textbf{330$\Omega$:} $\square$ R4, $\square$ R5, $\square$ R6&\textbf{180$\Omega$:} $\square$ R16\\
    \textbf{4.7k$\Omega$:} $\square$ R14, $\square$ R15&\textbf{10$\Omega$:} $\square$ R12, $\square$ R13\\
    \textbf{10k$\Omega$:} $\square$ R9, $\square$ R7, $\square$ R10, $\square$ R11, $\square$ R8\\
\end{tabular}
\vspace{1em}

\subsection{Install Capacitors}

Collect and install the following capacitors. Values are also marked on silkscreen.

\begin{figure}[h!]
\includegraphics[width=0.6\textwidth]{caps.jpg}
\centering
\end{figure}

\vspace{1em}
\begin{tabular}{ p{8cm}p{5.5cm}}
    \textbf{0.22$\mu$F:} $\square$ C7, $\square$ C8, $\square$ C10\\
    \textbf{0.1$\mu$F:} $\square$ C1, $\square$ C3, $\square$ C6, $\square$ C11, $\square$ C15\\
    \textbf{1.0$\mu$F:} $\square$ C9, $\square$ C12, $\square$ C13, $\square$ C14\\
\end{tabular}
\vspace{1em}

After installing the resistors and capacitors your board should look like this.

\begin{figure}[h!]
\includegraphics[width=0.8\textwidth]{passive-install.jpg}
\centering
\end{figure}

\subsection{Back Side Components}

These miscellaneous parts make up most of the remainder of the system.

For the MOSFET ensure the match the body shape up with the silkscreen outline to get the pins the correct holes.

\begin{figure}[H]
\includegraphics[width=0.8\textwidth]{pot-sw-mic.jpg}
\centering
\end{figure}

\vspace{1em}
\begin{tabular}{ p{7cm}p{5.5cm}}
    \textbf{Microphone:} $\square$ MIC&\textbf{MOSFET:} $\square$ Q1\\
    \textbf{Potentiometers:} $\square$ TM1, $\square$ TM2, $\square$ TM2&\textbf{Switch:} $\square$ SW2\\
\end{tabular}
\vspace{1em}

\newpage

\subsection{Sockets and Jacks}

These parts interface with pluggable components and accessories.

\begin{figure}[h!]
\includegraphics[width=0.8\textwidth]{jack-sock.jpg}
\centering
\end{figure}

Make sure to match the notch on the DIP sockets with the notch on the silkscreen to prevent confusion when inserting the integrated circuits (ICs) that will go in them.

Some of the audio jacks have extra alignment bumps that do not fit in the board.
Feel free to trim the extra bumps off using the flush cutters to allow the jacks to sit flat on the board.
You may need to use some tape to hold the jacks in place while you solder them.

\vspace{1em}
\begin{tabular}{ p{10cm}p{5.5cm}}
    \textbf{DIP Sockets:} $\square$ IC2, $\square$ IC3, $\square$ IC4\\
    \textbf{Audio Jacks:} $\square$ J2, $\square$ J3, $\square$ J4\\
\end{tabular}

\subsection{Arduino}

The Arduino solders into place on the back of the board.
Ensure that the USB port faces the center of the board (matching the silk screen).

\begin{figure}[h!]
\includegraphics[width=0.7\textwidth]{arduino.jpg}
\centering
\end{figure}

\vspace{1em}
\begin{tabular}{ p{10cm}p{5.5cm}}
    \textbf{Arduino:} $\square$ Arduino\\
\end{tabular}
\vspace{1em}

\subsection{Speaker}

Some assembly is required before installing the speaker.

\begin{figure}[h!]
\includegraphics[width=0.6\textwidth]{spkr-asm.jpg}
\centering
\end{figure}

Attach two short leads of insulated wire (magnet wire or wire wrap wire is ideal) to the pads on the back of the speaker.
The speaker is magnetic, so it will stick to your soldering iron!
Tape the speaker down to your work surface while attaching the leads to avoid problems.

Installation onto the board can be accomplished with double-sided tape or a small dab of hot glue.
If building an enclosure you may want to leave the leads long, or wait to install the speaker until wires can be sized for mounting.

\vspace{1em}
\begin{tabular}{ p{10cm}p{5.5cm}}
    \textbf{Speaker:} $\square$ Speaker\\
\end{tabular}
\vspace{1em}

So now your board should look something like this

\begin{figure}[H]
\includegraphics[width=0.8\textwidth]{back-pre-plug.jpg}
\centering
\end{figure}

\subsection{Front Side Components} The user interface components

\begin{figure}[H]
\includegraphics[width=0.8\textwidth]{led-enc-2.jpg}
\centering
\end{figure}

Polarity matters for LEDs!
Install with long lead in positive hole.
Feel free to choose different colors than those recommended below.

Note that all of these components go on the front side of the board.

\vspace{1em}
\begin{tabular}{ p{9cm}p{5.5cm}}
    \textbf{Display Header (16 pins):} $\square$ Front Header&\textbf{Encoder:} $\square$ S1\\
    \textbf{LEDs:} $\square$ Power (Red), $\square$ CW RX (Blue), $\square$ Scope (Green)\\
\end{tabular}
\vspace{1em}

Once you solder in the display header the display can be plugged in and secured with provided standoffs.

\begin{figure}[h!]
\includegraphics[width=0.8\textwidth]{display.jpg}
\centering
\end{figure}

\newpage

\subsection{Socketed ICs} The final step.

You will now insert the following ICs into their respective sockets.
It is important to match pin 1 of the IC with pin 1 on the sockets.
This can be accomplished by matching the semicircular marking on the IC package with the cutout on the socket.

\vspace{1em}
\begin{tabular}{ p{9cm}p{5.5cm}}
    \textbf{MCP41010:} $\square$ IC2\\
    \textbf{LM567N:} $\square$ IC3\\
    \textbf{LF353N:} $\square$ IC4
\end{tabular}

\vspace{1em}
\begin{figure}[h!]
\includegraphics[width=0.8\textwidth]{back-install-2.jpg}
\centering
\end{figure}
\vspace{1em}

\newpage

\section{Initial Power On and Calibration}

Measure resistance between power and ground (IC1 pin 3 and tab) and verify that it is large (greater than a couple thousand Ohms).

\subsection{Display}

Connect a 7-15V power supply and turn on SW1 (slide towards center of board).
You should see the power LED come on, and after a few seconds you may see some text on the display.
Adjust the contrast of the display by rotating the Contrast pot (TM3) until the text on the display is clear.

\vspace{1em}
\begin{figure}[h!]
\includegraphics[width=0.8\textwidth]{display-on.jpg}
\centering
\end{figure}

\subsection{Audio Gain}

The two audio inputs to the board have independent gain (aka volume) adjustments via labeled potentiometers on the back of the device.
Start with both pots in the 2 o'clock position as a starting point.

In order to tune the microphone gain place the Input Select switch in the MIC position and play some Morse code on your computer or phone.
A good source for this are the audio files provided by W1AW for Morse code training.
Start with slower than 20 WPM code while tuning.

With the device on, push the encoder (S1) on the front panel in towards to board to make it click.
You will see a menu on the display.
Rotate the knob until you see the menu item for "Perform Tone Sweep" and click the encoder in again.
Now while the device scans to detect the CW signal you are playing, slowly adjust the Mic Level pot.
Once the Scope Pin and CW RX LEDs start lighting in time with the playing signal you have it tuned!

Repeat the same procedure with an audio source on the Line In port using a standard 1/8" audio cable.
Ensure that the Input Select switch is in the LINE position and that you adjust gain on the Line Level pot.

\end{document}
