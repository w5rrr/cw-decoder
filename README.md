CW Decoder
==========

This PCB is a standalone Morse Code decoder and keyer.

Todo
----

* [ ] Check in PCB files
* [ ] Check in firmware

Build The Docs
--------------

    cd doc
    pdflatex w5rrr-morse-decoder.tex
